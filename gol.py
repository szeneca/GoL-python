import sys
import time

from random import randint

class cell:
    """Cell class"""

    def __init__(self, state):
        self.state = state

    def alive(self):
        return self.state

    def setL(self, L):
        self.state = L

class board:
    """Board class"""

    def __init__(self, m, n):
        self.rows = m
        self.cols = n

        #default initialization for board (all dead cells)
        self.board = [[cell(0) for i in range(n)] for j in range(m)]

    def setL(self, i, j, L):
        """Provide a simpler method for setting value of cell (i, j)"""
        self.board[i][j].setL(L)

    def display(self):
        """Display board in stdout"""
        
        for i in range(self.rows):
            for j in range(self.cols):

                #remove auto-spacing so cells appear close to each other
                sys.stdout.softspace=0

                #if cell is aive, print it with colour, else print in normal text
                if self.board[i][j].alive():
                    print "\033[0;31m"+"0"+"\033[0m",
                else:
                    print "0",

            #print newline at end of each row
            print ""

        #reset spacing to default
        sys.stdout.softspace=1

    #file format
    #
    #   filename.csv
    #   =========FILE_BEGIN=========
    #   self.rows,
    #   self.cols,
    #   self.board[0][0],       ,...,     ,self.board[0][self.cols],
    #           .                               .
    #           .                               .
    #           .                               .
    #   self.board[self.rows],  ,...,     ,self.board[self.rows][self.cols],
    #   =========FILE_END===========
    #

    #export board to .csv
    def exportCSV(self, filename):
        """Export board to .csv file"""

        #filename formatting
        name = filename + ".csv"

        #create file
        f = open(name, 'w')

        #write board dimensions
        f.write(str(self.rows) + '\n')
        f.write(str(self.cols) + '\n')

        #write board contents
        for i in range(self.rows):
            for j in range(self.cols):
                f.write(str(self.board[i][j].alive()) + ',')
            f.write('\n')

    #populate board from .csv
    def importCSV(self, filename):
        """Import board from .csv file"""

        #format filename
        name = filename + ".csv"

        #open file
        f = open(name, 'r')

        #read in board dimensions
        self.rows = int(f.readline())
        self.cols = int(f.readline())

        #read in board contents
        for i in range(self.rows):
            line = f.readline()
            line = line.split(',')

            for j in range(self.cols):
                self.board[i][j].setL(int(line[j]))


    #creating patterns at specific locations

    #STATICS
    def block(self, i, j):
        """Place a block on the board with top left corner at the cell (i, j)"""

        #range check

        #place block at (i, j)
        for a in range(0,2):
            for b in range(0,2):
                self.board[i+a][j+b].setL(1)

    def beehive(self, i, j, o):
        """Insert beehive pattern, top left corner at cell (i, j), orientation controlled by o"""

        #range check

        if(o == 0):
            self.board[i][j+1].setL(1)
            self.board[i][j+2].setL(1)
            self.board[i+1][j].setL(1)
            self.board[i+1][j+3].setL(1)
            self.board[i+2][j+1].setL(1)
            self.board[i+2][j+2].setL(1)
        else:
            self.board[i][j+1].setL(1)
            self.board[i+1][j].setL(1)
            self.board[i+1][j+2].setL(1)
            self.board[i+2][j].setL(1)
            self.board[i+2][j+2].setL(1)
            self.board[i+3][j+1].setL(1)


    def loaf(self, i, j):
        pass

    def boat(self, i ,j):
        pass

    #OSCILLATORS
    def blinker(self, i, j, orientation):
        """Place a blinker on the board at cell (i, j) """

        #range check

        #orientation == 1 -> vertical
        if(orientation):
            for k in range(3):
                self.board[i+k][j].setL(1)
        #orientation == 0 -> horizontal
        else:
            for k in range(3):
                self.board[i][j+k].setL(1)

    def toad(self, i, j):
        pass

    def beacon(self, i, j):
        pass

    def pulsar(self, i, j):
        """insert pulsar with top left corner at (i, j)"""

        #range check

        #place pattern on board
        a = [0,5,7,12]
        b = [2,3,4,8,9,10]
        c = [0,5,7,12]

        for k in a:
            for x in range(2,5):
                self.board[i+k][j+x].setL(1)

            for y in range(8,11):
                self.board[i+k][j+y].setL(1)

        for k in b:
            for z in c:
                self.board[i+k][j+z].setL(1)


    def penta(self, i, j):
        pass

    #SPACESHIPS
    def glider(self, i, j):
        """gosper glider"""

        #range check

        #travels diagonally right
        self.board[i][j+2].setL(1)
        self.board[i+1][j].setL(1)
        self.board[i+1][j+2].setL(1)
        self.board[i+2][j+1].setL(1)
        self.board[i+2][j+2].setL(1)

    def lwss(self, i, j):
        """lightweight space ship"""
        #A.K.A. c/2 orthogonal

        self.board[i][j].setL(1)
        self.board[i][j+3].setL(1)
        self.board[i+1][j+4].setL(1)
        self.board[i+2][j].setL(1)
        self.board[i+2][j+4].setL(1)
        self.board[i+3][j+1].setL(1)
        self.board[i+3][j+2].setL(1)
        self.board[i+3][j+3].setL(1)
        self.board[i+3][j+4].setL(1)

        #travels right
        pass

    #randomly populate the entire board
    def fillRandom(self):
        """Randomly populate entire board"""
        for i in range(self.rows):
            for j in range(self.cols):
                self.board[i][j].setL(randint(0,1))

    #randomly populate an m-by-n rectangular section of the board, starting at cell (i, j)
    def fillSectionRandom(self, i, j, m, n):
        """Randomly populate an m-by-n section of the board, starting at cell (i, j)"""

        #range check
        if( (i+m > self.rows) or (j+n > self.cols)):
            print "ERROR: in fillRandomSection(): random section extent exceeds board dimensions"
            sys.exit(1)

        #randomly init cells in section
        for a in range(m):
            for b in range(n):
                self.board[i+a][j+b].setL(randint(0,1))


def cellSurvives(i, j, count, currGen, nextGen):
    """Given a cell position and it's live neighbour count, update the nextGen board"""
    if(currGen.board[i][j].alive()):
        #cell is alive
        if(count == 2 or count == 3):
            nextGen.board[i][j].setL(1)
        else:
            nextGen.board[i][j].setL(0)
    else:
        #cell is dead
        if(count == 3):
            nextGen.board[i][j].setL(1)
        else:
            nextGen.board[i][j].setL(0)     #<-- possibly redundant, could remove


def normalEval(currGen, nextGen):
    """Process currGen board and update cells in nextGen board, using hard edge conditions"""

    #sanity check that boards are equal dimension
    if( (currGen.rows != nextGen.rows) or (currGen.cols != nextGen.cols)):
        print "ERROR: in normalEval(): board dimensions are mismatched"
        exit(1)

    #alias to dummy variables with easier short names
    m = currGen.rows - 1
    n = currGen.cols - 1

    #need to undo the biasing above for the loop ranges
    for i in range(m+1):
        for j in range(n+1):

            #reset Moore neighbour count
            count = 0

            #centre
            if( (0<i<m) and (0<j<n) ):
                #for the cell at (i,j), get number of living neighbours
                for a in range(-1,2):
                    for b in range(-1,2):
                        if(a==0 and b ==0):
                            pass
                        else:
                            count += currGen.board[i+a][j+b].alive()
                #determine if cell (i, j) lives based on count, update nextGen board
                cellSurvives(i, j, count, currGen, nextGen)

            #top edge
            if( (i==0) and (0<j<n) ):
                #nextGen.board[i][j].setL(1)
                for a in range(2):
                    for b in range(-1,2):
                        if(a==0 and b ==0):
                            pass
                        else:
                            count += currGen.board[i+a][j+b].alive()

                #determine if cell (i, j) lives based on count, update nextGen board
                cellSurvives(i, j, count, currGen, nextGen)

            #left edge
            if( (0<i<m) and (j==0) ):
                #nextGen.board[i][j].setL(1)
                for a in range(-1,2):
                    for b in range(2):
                        if(a==0 and b ==0):
                            pass
                        else:
                            count += currGen.board[i+a][j+b].alive()

                #determine if cell (i, j) lives based on count, update nextGen board
                cellSurvives(i, j, count, currGen, nextGen)

            #right edge
            if( (0<i<m) and (j==n) ):
                #nextGen.board[i][j].setL(1)
                for a in range(-1,2):
                    for b in range(-1,1):
                        if(a==0 and b ==0):
                            pass
                        else:
                            count += currGen.board[i+a][j+b].alive()

                #determine if cell (i, j) lives based on count, update nextGen board
                cellSurvives(i, j, count, currGen, nextGen)

            #bottom edge
            if( (i==m) and (0<j<n) ):
                #nextGen.board[i][j].setL(1)
                for a in range(-1,1):
                    for b in range(-1,2):
                        if(a==0 and b ==0):
                            pass
                        else:
                            count += currGen.board[i+a][j+b].alive()

                #determine if cell (i, j) lives based on count, update nextGen board
                cellSurvives(i, j, count, currGen, nextGen)

            #top left corner
            if( (i==0) and (j==0) ):
                #nextGen.board[i][j].setL(1)

                #for the cell at (i,j), get number of living neighbours check
                for a in range(2):
                    for b in range(2):
                        if(a==0 and b ==0):
                            pass
                        else:
                            count += currGen.board[i+a][j+b].alive()

                #determine if cell (i, j) lives based on count, update nextGen board
                cellSurvives(i, j, count, currGen, nextGen)

            #top right corner
            if( (i==0) and (j==n) ):
                #nextGen.board[i][j].setL(1)
                for a in range(2):
                    for b in range(-1,1):
                        if(a==0 and b ==0):
                            pass
                        else:
                            count += currGen.board[i+a][j+b].alive()

                #determine if cell (i, j) lives based on count, update nextGen board
                cellSurvives(i, j, count, currGen, nextGen)

            #bottom left corner
            if( (i==m) and (j==0) ):
                #nextGen.board[i][j].setL(1)
                for a in range(-1,1):
                    for b in range(2):
                        if(a==0 and b ==0):
                            pass
                        else:
                            count += currGen.board[i+a][j+b].alive()

                #determine if cell (i, j) lives based on count, update nextGen board
                cellSurvives(i, j, count, currGen, nextGen)

            #bottom right corner
            if( (i==m) and (j==n) ):
                #nextGen.board[i][j].setL(1)
                for a in range(-1,1):
                    for b in range(-1,1):
                        if(a==0 and b ==0):
                            pass
                        else:
                            count += currGen.board[i+a][j+b].alive()

                #determine if cell (i, j) lives based on count, update nextGen board
                cellSurvives(i, j, count, currGen, nextGen)

def toroidEval(currGen, nextGen):
    """Process currGen board and update cells in nextGen board, using toroidal edge conditions"""
    
    #sanity check that boards are equal dimension
    if( (currGen.rows != nextGen.rows) or (currGen.cols != nextGen.cols)):
        print "ERROR: in normalEval(): board dimensions are mismatched"
        exit(1)

    #alias to dummy variables with shorter names
    m = currGen.rows - 1
    n = currGen.cols - 1

    #need to undo the biasing above for the loop ranges
    for i in range(m+1):
        for j in range(n+1):

            #reset Moore neighbour count
            count = 0

            #centre
            if( (0<i<m) and (0<j<n) ):
                for a in range(-1,2):
                    for b in range(-1,2):
                        if(a==0 and b ==0):
                            pass
                        else:
                            #currGen.board[i+a][j+b].setL(1) #DEBUG
                            count += currGen.board[i+a][j+b].alive()

                #determine if cell (i, j) lives based on count, update nextGen board
                cellSurvives(i, j, count, currGen, nextGen)

            #top edge
            if( (i==0) and (0<j<n) ):
                for b in range(-1,2):
                    count += currGen.board[m][j+b].alive()

                    for a in range(2):
                        if ( a == 0 and b == 0):
                            pass
                        else:
                            count += currGen.board[i+a][j+b].alive()                     

                #determine if cell (i, j) lives based on count, update nextGen board
                cellSurvives(i, j, count, currGen, nextGen)

            #left edge
            if( (0<i<m) and (j==0) ):
                for a in range(-1,2):
                    count += currGen.board[i+a][n].alive()

                    for b in range(2):
                        if ( a == 0 and b == 0):
                            pass
                        else:
                            count += currGen.board[i+a][j+b].alive()

                #determine if cell (i, j) lives based on count, update nextGen board
                cellSurvives(i, j, count, currGen, nextGen)

            #right edge
            if( (0<i<m) and (j==n) ):
                for a in range(-1,2):
                    count += currGen.board[i+a][0].alive()

                    for b in range(-1,1):
                        if (a == 0 and b == 0):
                            pass
                        else:
                            count += currGen.board[i+a][j+b].alive()

                #determine if cell (i, j) lives based on count, update nextGen board
                cellSurvives(i, j, count, currGen, nextGen)

            #bottom edge
            if( (i==m) and (0<j<n) ):
                for b in range(-1,2):
                    count += currGen.board[0][j+b].alive()

                    for a in range(-1,1):
                        if( a == 0 and b == 0):
                            pass
                        else:
                            count += currGen.board[i+a][j+b].alive()

                #determine if cell (i, j) lives based on count, update nextGen board
                cellSurvives(i, j, count, currGen, nextGen)

            #top left corner
            if( (i==0) and (j==0) ):
                count += currGen.board[m][n].alive()

                for a in range(2):
                    count += currGen.board[m][a].alive()
                    count += currGen.board[a][n].alive()

                    for b in range(2):
                        if(a == 0 and b == 0):
                            pass
                        else:
                            count += currGen.board[i+a][j+b].alive()

                #determine if cell (i, j) lives based on count, update nextGen board
                cellSurvives(i, j, count, currGen, nextGen)

            #top right corner
            if( (i==0) and (j==n) ):
                count += currGen.board[m][0].alive()
                count += currGen.board[m][j].alive()
                count += currGen.board[m][j-1].alive()

                for a in range(2):
                    count += currGen.board[i+a][0].alive() 
                    
                    for b in range(-1,1):
                        if(a == 0 and b == 0):
                            pass
                        else:
                            count += currGen.board[i+a][j+b].alive() 

                #determine if cell (i, j) lives based on count, update nextGen board
                cellSurvives(i, j, count, currGen, nextGen)

            #bottom left corner
            if( (i==m) and (j==0) ):
                count += currGen.board[0][n].alive()
                count += currGen.board[0][j].alive()
                count += currGen.board[0][j+1].alive()

                for a in range(-1,1):
                    count += currGen.board[i+a][n].alive()

                    for b in range(2):
                        if(a == 0 and b == 0):
                            pass
                        else:
                            count += currGen.board[i+a][j+b].alive()
                
                #determine if cell (i, j) lives based on count, update nextGen board
                cellSurvives(i, j, count, currGen, nextGen)

            #bottom right corner
            if( (i==m) and (j==n) ):
                count += currGen.board[0][0].alive()

                for a in range(-1,1):
                    count += currGen.board[i+a][0].alive()
                    count += currGen.board[0][j+a].alive()

                    for b in range(-1,1):
                        if(a == 0 and b == 0):
                            pass
                        else:
                            count += currGen.board[i+a][j+b].alive()

                #determine if cell (i, j) lives based on count, update nextGen board
                cellSurvives(i, j, count, currGen, nextGen)


def clearScreen():
    """Clear the screen on ANSI compliant terminals"""

    #clear screen, put cursor in top left position
    sys.stderr.write("\033[2J\033[H")

def wait(n):
    """wait n milliseconds"""
    time.sleep(0.001*n)

def playGameForN(board1, board2, mode, n, delay):
    """Given initial board and a second board play Game of Life for N generations"""

    #hide cursor
    sys.stderr.write("\033[?25l")

    for k in range(n):
        clearScreen()
        if(k % 2 == 0):
            board1.display()
            if (mode == 't'):
                toroidEval(board1, board2)
            else:
                normalEval(board1, board2)
        else:
            board2.display()
            if (mode == 't'):
                toroidEval(board2, board1)
            else:
                normalEval(board2, board1)
        wait(delay)

    #show cursor
    sys.stderr.write("\033[?25h")

m = 17
n = 99

a = board(m,n)
b = board(m,n)

#a.beehive(1,1,0)
#a.beehive(5,5,1)

#a.lwss(3,10)

#a.display()

o = 3

a.pulsar(2,o)
a.pulsar(2,o+16)
a.pulsar(2,o+2*16)
a.pulsar(2,o+3*16)
a.pulsar(2,o+4*16)
a.pulsar(2,o+5*16)

a.exportCSV("pulsars")

playGameForN(a, b, 't',  50, 100)

#a = board(m,n)

#a.glider(1,1)
#a.glider(1,11)
#a.glider(1,21)
#a.glider(1,31)
#a.glider(1,41)
#playGameForN(a,b, 't', 500, 100)
