GoL-Python
==========

A not-quite simple implementation of Conway's Game of Life in Python.

Features:
---------

- Arbitrary rectangular board dimensions
- Easy common pattern placement (statics, gliders, and oscillators)
- Toroidal or non-toroidal boundary evaluation
- Colour output

To Do:
------
 - Comments!
 - some pattern implementations
 - Output colour configuration
 - Misc other functionality